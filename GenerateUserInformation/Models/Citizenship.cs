namespace GenerateUserInformation.Models
{
    public enum Citizenship
    {
        Lithuanian, 
        Portuguese,
        Belarusian
    }
}
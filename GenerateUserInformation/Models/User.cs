using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.ComponentModel.DataAnnotations;
using GenerateUserInformation.Services;

namespace GenerateUserInformation.Models
{
    public class User
    {
        public Guid Id { get; set; }
        
        [MaxLength(12, ErrorMessage = "Maximum number of characters that can be entered is 12!")]
        public string Name { get; set; }
        
        [MaxLength(18, ErrorMessage = "Maximum number of characters that can be entered is 18!")]
        public string Surname { get; set; }
        
        [RegularExpression(@"^\d{11}$", ErrorMessage = "Invalid code format!")]
        public string LT_personal_code { get; set; }
        
        [RegularExpression(@"^([\+]?(?:00)?[0-9]{1,3}[\s.-]?[0-9]{1,12})([\s.-]?[0-9]{1,4}?)$", ErrorMessage = "Invalid number format!")]
        public string Phone_number { get; set; }
        
        public int Age { get; set; }
        
        [ValidateBirthday]
        [DataType(DataType.DateTime, ErrorMessage = "Invalid date format!"), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Birthdate { get; set; }
        public Gender Gender { get; set; }
        public Citizenship Citizenship { get; set; }
        public string GenderStr { get; set; }
        public string CitizenshipStr { get; set; }

        public void GenderToString()
        {
            GenderStr = Gender.ToString();
        }

        public void CitizenshipToString()
        {
            CitizenshipStr = Citizenship.ToString();
        }

        public void StringToGender()
        {
            if(GenderStr.Equals(Gender.Male.ToString()))
            {
                this.Gender = Gender.Male;
            }
            else
            {
                this.Gender = Gender.Female;
            }
        }

        public void StringToCitizenship()
        {
            switch(CitizenshipStr)
            {
                case "Lithuanian" :
                    this.Citizenship = Citizenship.Lithuanian;
                break;
                case "Portuguese" :
                    this.Citizenship = Citizenship.Portuguese;
                break;
                case "Belarusian" :
                    this.Citizenship = Citizenship.Belarusian;
                break;
            }
        }
    }
}
        
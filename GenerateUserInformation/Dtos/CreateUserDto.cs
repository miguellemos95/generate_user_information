using System;
using System.ComponentModel.DataAnnotations;
using GenerateUserInformation.Models;
using GenerateUserInformation.Services;

namespace GenerateUserInformation.Dtos
{
    public class CreateUserDto
    {
        [MaxLength(12, ErrorMessage = "Maximum number of characters that can be entered is 12!")]
        public string Name { get; set; }
        
        [MaxLength(18, ErrorMessage = "Maximum number of characters that can be entered is 18!")]
        public string Surname { get; set; }
        
        [RegularExpression(@"^\d{11}$", ErrorMessage = "Invalid code format!")]
        public string LT_personal_code { get; set; }
        
        [RegularExpression(@"^([\+]?(?:00)?[0-9]{1,3}[\s.-]?[0-9]{1,12})([\s.-]?[0-9]{1,4}?)$", ErrorMessage = "Invalid number format!")]
        public string Phone_number { get; set; }
        
        [ValidateBirthday]
        [DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Birthdate { get; set; }
        public int Age { get; set; }
        public Gender Gender { get; set; }
        public Citizenship Citizenship { get; set; }
    }
}
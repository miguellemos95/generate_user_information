using AutoMapper;
using GenerateUserInformation.Dtos;
using GenerateUserInformation.Models;

namespace GenerateUserInformation.Profiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, GetUserDto>();
            CreateMap<CreateUserDto, User>();
        }
    }
}
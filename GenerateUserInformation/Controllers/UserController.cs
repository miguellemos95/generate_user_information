using GenerateUserInformation.Models;
using GenerateUserInformation.Data.Repositories;
using GenerateUserInformation.Services;
using GenerateUserInformation.Dtos;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using AutoMapper;

namespace GenerateUserInformation.Controllers
{
    [Route("users")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserRepository _UserRepository;
        private readonly IMapper _Mapper;
        private static bool GenerateUsers = true;

        public UserController(IUserRepository UserRepository, IMapper Mapper)
        {
            _UserRepository = UserRepository;

            if(GenerateUsers)
            {
                _UserRepository.GenerateUsers();
                GenerateUsers= false;
            }

            _Mapper = Mapper;
        }

        // GET /Users
        [HttpGet]
        public ActionResult<GetUserDto> GetUsers()
        {
            var Users = _UserRepository.GetUsers();
            
            return Ok(_Mapper.Map<IEnumerable<GetUserDto>>(Users));
        }

        // GET /Users/{id}
        [HttpGet("{id}")]
        public ActionResult<GetUserDto> GetUser(Guid id)
        {
            User User = _UserRepository.GetUser(id);

            if (User is null)
            {
                return NotFound();
            }

            return Ok(_Mapper.Map<GetUserDto>(User));
        }

        // POST /Users
        [HttpPost]
        public ActionResult<GetUserDto> CreateUser(CreateUserDto CreateUserDto)
        {
            User User = _Mapper.Map<User>(CreateUserDto);
            
            if (User is null)
            {
                return NotFound();
            }

            User.Id = Guid.NewGuid();
            User.CitizenshipToString();
            User.GenderToString();

            _UserRepository.CreateUser(User);

            return Ok(_Mapper.Map<GetUserDto>(User));
        }
    }
}
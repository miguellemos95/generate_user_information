using GenerateUserInformation.Models;
using System.Collections.Generic;
using System;
namespace GenerateUserInformation.Data.Repositories
{
    public interface IUserRepository
    {
        User GetUser(Guid id);
        ICollection<User> GetUsers();
        void CreateUser(User User);
        ICollection<User> GenerateUsers();
    }
}
using GenerateUserInformation.Models;
using System.Collections.Generic;
using System;
using System.Linq;

namespace GenerateUserInformation.Data.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly List<User> Users = new List<User>();
        private readonly IGenerateUserInfo generateUserInfo;

        public UserRepository(IGenerateUserInfo _generateUserInfo)
        {
            generateUserInfo = _generateUserInfo;
        }

        public ICollection<User> GenerateUsers()
        {
            int numberUsersToGenerate = 10;
            
            for(int i = 0; i < numberUsersToGenerate; i++)
            {
                User user = generateUserInfo.GenerateUser(i);
                if(IsUnique(user))
                {
                   Users.Add(user);
                }
            }
            
            return Users;
        }

        public void CreateUser(User User)
        {
            if(IsUnique(User))
            {
                Users.Add(User);
            }
        }

        public User GetUser(Guid id)
        {
            return Users.Where(User => User.Id == id).SingleOrDefault(); 
        }

        public ICollection<User> GetUsers()
        {
            return Users;
        }

        public bool IsUnique(User User)
        {
            var index = Users.FindIndex(existingUser => existingUser.LT_personal_code == User.LT_personal_code || existingUser.Phone_number == User.Phone_number); 
            if(index == -1)
            {
                return true;
            }

            return false;
        }
    }
}
using System;
using GenerateUserInformation.Data.Repositories;
using GenerateUserInformation.Models;

namespace GenerateUserInformation.Services
{
    public class GenerateUserInfo : IGenerateUserInfo
    {
        public GenerateUserInfo(){}

        public User GenerateUser(int i)
        {
            DateTime birth = GenerateBirthdate(); 
            Citizenship citizenship = GenerateCitizenship();
            Gender gender = GenerateGender();
            
            User user = new User() 
                            {   
                                Name = "Name " + i, 
                                Surname = "Surname " + i, 
                                LT_personal_code = GenerateLTPersonalCode(), 
                                Phone_number = "+" + GeneratePhoneNumber(), 
                                Birthdate = birth.Date, 
                                Age = CalcAge(birth), 
                                Gender = gender,
                                Citizenship = citizenship,
                                CitizenshipStr = citizenship.ToString(),
                                GenderStr = gender.ToString()
                            };

            return user;
        }

        public Citizenship GenerateCitizenship()
        {
            Citizenship citizenship = (Citizenship) new Random().Next(0, Enum.GetNames(typeof(Citizenship)).Length);
            
            return citizenship;
        }


        public Gender GenerateGender()
        {
            Gender gender = (Gender) new Random().Next(0, Enum.GetNames(typeof(Gender)).Length);
            
            return gender;
        }

        public int CalcAge(DateTime birth)
        {
            DateTime now = DateTime.Today;
            int age = now.Year - birth.Year;

            if (now < birth.AddYears(age)) 
            {
                age--;
            }

            return age;
        }

        public string GeneratePhoneNumber()
        {
            var random = new Random();
            string phoneNumber = string.Empty;
            int numberDigits = 12;

            for (int i = 0; i < numberDigits; i++)
            {
                phoneNumber = String.Concat(phoneNumber, random.Next(10).ToString());
            }
               
            return phoneNumber;
        }

        public string GenerateLTPersonalCode() {
            var random = new Random();
            string ltPersonalCode = string.Empty;
            int numberDigits = 11;

            for (int i = 0; i < numberDigits; i++)
            {
                ltPersonalCode = String.Concat(ltPersonalCode, random.Next(10).ToString());
            }
               
            return ltPersonalCode;
        }

        public DateTime GenerateBirthdate()
        {
            DateTime to = DateTime.Today;
            DateTime from = to.AddYears(-120);
            Random rnd = new Random();
            var range = to.Date - from.Date;

            var randTimeSpan = new TimeSpan((long)(rnd.NextDouble() * range.Ticks)); 

            return from + randTimeSpan;
        }
    }
}

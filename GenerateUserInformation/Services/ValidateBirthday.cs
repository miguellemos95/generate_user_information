using System;
using System.ComponentModel.DataAnnotations;
using GenerateUserInformation.Dtos;

namespace GenerateUserInformation.Services
{
    public class ValidateBirthday : ValidationAttribute
    {
        public ValidateBirthday(){}

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var userDto = (CreateUserDto) validationContext.ObjectInstance;
            
            if(userDto.Birthdate >= DateTime.Today)
            {
                return new ValidationResult("Birthday has to be in the past!");
            }

            userDto.Age = CalcAge(userDto.Birthdate);
            
            return ValidationResult.Success;
        }

        public int CalcAge(DateTime birth)
        {
            DateTime now = DateTime.Today;
            int age = now.Year - birth.Year;

            if(age < 0)
            {
                return -1;
            }

            if (now < birth.AddYears(age)) 
            {
                age--;
            }

            return age;
        }
    }
}
using GenerateUserInformation.Models;
using System.Collections.Generic;
using System;

namespace GenerateUserInformation.Data.Repositories
{
    public interface IGenerateUserInfo
    {
        User GenerateUser(int i);
        Citizenship GenerateCitizenship();
        Gender GenerateGender();
        int CalcAge(DateTime birth);
        string GeneratePhoneNumber();
        string GenerateLTPersonalCode();
        DateTime GenerateBirthdate();
    }
}
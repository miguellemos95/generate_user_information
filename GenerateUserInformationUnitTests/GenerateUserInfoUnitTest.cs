using System;
using Xunit;
using GenerateUserInformation.Models;
using GenerateUserInformation.Services;
using GenerateUserInformation.Data.Repositories;
using System.Collections.Generic;
using Moq;

namespace GenerateUserInformationUnitTests
{
    public class UserRepositoryUnitTest
    {
        private User User1 = new User(){
                                Name = "Name",
                                Surname = "Surname",
                                LT_personal_code = "01756106168",
                                Phone_number = "+348351233713",
                                Age = 0,
                                Birthdate = DateTime.Today,
                                GenderStr = "Male",
                                CitizenshipStr = "Portuguese"
                            };

        private User User2 = new User(){
                                Name = "Name",
                                Surname = "Surname",
                                LT_personal_code = "01756106167",
                                Phone_number = "+348351233712",
                                Age = 0,
                                Birthdate = DateTime.Today,
                                GenderStr = "Male",
                                CitizenshipStr = "Portuguese"
                            };

        [Fact]
        public void TestAddUserSameLT_Code()
        {
            var generateUserInfo = new GenerateUserInfo();  
            var userRepository = new UserRepository(generateUserInfo);

            //add 2 objects with different LT_personal_code will SUCCESS 
            userRepository.CreateUser(User1);
            Assert.Equal(userRepository.GetUsers().Count, 1);

            userRepository.CreateUser(User2);
            Assert.Equal(userRepository.GetUsers().Count, 2);

            //add an object with same LT_personal_code of other users will NOT SUCCESS 

            User User3 = new User(){
                                Name = "Name",
                                Surname = "Surname",
                                LT_personal_code = "01756106168",
                                Phone_number = "+348351233710",
                                Age = 0,
                                Birthdate = DateTime.Today,
                                GenderStr = "Male",
                                CitizenshipStr = "Portuguese"
                            };
                            
            userRepository.CreateUser(User3);
            Assert.Equal(userRepository.GetUsers().Count, 2);
        }

        [Fact]
        public void TestAddUserSamePhone()
        {
            var generateUserInfo = new GenerateUserInfo();  
            var userRepository = new UserRepository(generateUserInfo);

            //add 2 objects with different phone number will SUCCESS 
            userRepository.CreateUser(User1);
            Assert.Equal(userRepository.GetUsers().Count, 1);

            userRepository.CreateUser(User2);
            Assert.Equal(userRepository.GetUsers().Count, 2);

            User User3 = new User(){
                                Name = "Name",
                                Surname = "Surname",
                                LT_personal_code = "01756106164",
                                Phone_number = "+348351233712",
                                Age = 0,
                                Birthdate = DateTime.Today,
                                GenderStr = "Male",
                                CitizenshipStr = "Portuguese"
                            };

            //add an object with same phone number of other users will NOT SUCCESS 
            userRepository.CreateUser(User3);
            Assert.Equal(userRepository.GetUsers().Count, 2);
        }
    }
}

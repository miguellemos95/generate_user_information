using System;
using Xunit;
using GenerateUserInformation.Dtos;
using GenerateUserInformation.Services;

namespace GenerateUserInformationUnitTests
{
    public class ValidateBirthdayUnitTest
    {
        [Fact]
        public void TestCalcAge()
        {
            var validateBirthday = new ValidateBirthday();
            DateTime dateTime = DateTime.Today;

            var result = validateBirthday.CalcAge(dateTime);
            var expResult = 0;

            Assert.Equal(result, expResult);

            dateTime = DateTime.Today.AddYears(-5);
            result = validateBirthday.CalcAge(dateTime);
            expResult = 5;

            Assert.Equal(result, expResult);

            dateTime = DateTime.Today.AddYears(5);
            result = validateBirthday.CalcAge(dateTime);
            expResult = -1;

            Assert.Equal(result, expResult);
        }
    }
}
